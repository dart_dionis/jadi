//
//  DSViewController.m
//  Jaday
//
//  Created by Denis Andreev on 16.08.13.
//  Copyright (c) 2013 Denis Andreev. All rights reserved.
//

#import "DSViewController.h"

@interface DSViewController ()

@end

@implementation DSViewController

@synthesize loginButtonCust;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    UIImage *greenButtonImage = [UIImage imageNamed:@"redButton.png"];
    UIImage *stretchableGreenButton = [greenButtonImage stretchableImageWithLeftCapWidth:12 topCapHeight:0];
    [loginButtonCust setBackgroundImage:stretchableGreenButton forState:UIControlStateNormal];
    UIImage *darkGreenButtonImage = [UIImage imageNamed:@"redButtonActivated.png"];
    UIImage *stretchabledarkGreenButton = [darkGreenButtonImage stretchableImageWithLeftCapWidth:12 topCapHeight:0];
    [loginButtonCust setBackgroundImage:stretchabledarkGreenButton forState:UIControlStateHighlighted];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)loginButton:(id)sender {
    // function for login and get new Form
}
@end
