//
//  main.m
//  Jaday
//
//  Created by Denis Andreev on 16.08.13.
//  Copyright (c) 2013 Denis Andreev. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DSAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DSAppDelegate class]));
    }
}
