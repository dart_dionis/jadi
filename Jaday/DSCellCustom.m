//
//  DSCellCustom.m
//  Jaday
//
//  Created by Denis Andreev on 19.08.13.
//  Copyright (c) 2013 Denis Andreev. All rights reserved.
//

#import "DSCellCustom.h"

@implementation DSCellCustom
@synthesize  jediImage, jediName;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
@end
