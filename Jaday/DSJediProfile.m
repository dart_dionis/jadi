//
//  DSJediProfile.m
//  Jaday
//
//  Created by Denis Andreev on 20.08.13.
//  Copyright (c) 2013 Denis Andreev. All rights reserved.
//

#import "DSJediProfile.h"

@interface DSJediProfile ()

@end

@implementation DSJediProfile

@synthesize jediName, jediNameText, jediInfo, jediInfoText, jediPicture, picName;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	jediName.title = jediNameText;
    jediPicture.image = [UIImage imageNamed:picName];
    jediInfo.text = jediInfoText;
    jediInfo.showsVerticalScrollIndicator=true;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
