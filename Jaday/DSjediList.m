//
//  DSjediList.m
//  Jaday
//
//  Created by Denis Andreev on 19.08.13.
//  Copyright (c) 2013 Denis Andreev. All rights reserved.
//

#import "DSjediList.h"
#import "DSCellCustom.h"
#import "DSJediProfile.h"

@interface DSjediList ()

@end

@implementation DSjediList
@synthesize jediList, jediMuArrey;


-(void) viewDidLoad
{
    [super viewDidLoad];
    NSString* filename = [NSString stringWithFormat:@"jedi.json"];
    NSString *path = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:filename];
    
    NSURL *url      =[NSURL fileURLWithPath:path];
    NSData *data = [NSData dataWithContentsOfURL:url];
    if (data != nil) {
        NSError *error;
        jediMuArrey = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    }
    
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)jediList
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)jediList numberOfRowsInSection:(NSInteger)section
{
    return [jediMuArrey count];
}

-(DSCellCustom *)tableView:(UITableView *)jediList cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *Ident = @"jedi";
    DSCellCustom *cell =[self.jediList dequeueReusableCellWithIdentifier:Ident];
    
    if (cell == nil) {
        cell = [[DSCellCustom alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Ident];
    }
    NSUInteger row = indexPath.row;
    cell.jediImage.image = [UIImage imageNamed:[[jediMuArrey objectAtIndex:row] objectForKey:@"photo"]];
    cell.jediName.text = [[jediMuArrey objectAtIndex:row] objectForKey:@"jadiName"];
    return cell;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"jedayInfo"]){
        NSIndexPath *indexPath = [self.jediList indexPathForSelectedRow];
        NSUInteger row = indexPath.row;
        DSJediProfile *jediInformation = segue.destinationViewController;
        
        jediInformation.jediNameText = [[jediMuArrey objectAtIndex:row] objectForKey:@"jadiName"];
        jediInformation.picName = [[jediMuArrey objectAtIndex:row] objectForKey:@"photo"];
        jediInformation.jediInfoText = [[jediMuArrey objectAtIndex:row] objectForKey:@"info"];
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
