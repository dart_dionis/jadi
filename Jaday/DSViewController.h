//
//  DSViewController.h
//  Jaday
//
//  Created by Denis Andreev on 16.08.13.
//  Copyright (c) 2013 Denis Andreev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DSViewController : UIViewController
- (IBAction)loginButton:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *loginField;
@property (strong, nonatomic) IBOutlet UITextField *password;
@property (strong, nonatomic) IBOutlet UIButton *loginButtonCust;

@end
