//
//  DSAppDelegate.h
//  Jaday
//
//  Created by Denis Andreev on 16.08.13.
//  Copyright (c) 2013 Denis Andreev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
