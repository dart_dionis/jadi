//
//  DSCellCustom.h
//  Jaday
//
//  Created by Denis Andreev on 19.08.13.
//  Copyright (c) 2013 Denis Andreev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DSCellCustom : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *jediImage;
@property (strong, nonatomic) IBOutlet UILabel *jediName;

@end
