//
//  DSjediList.h
//  Jaday
//
//  Created by Denis Andreev on 19.08.13.
//  Copyright (c) 2013 Denis Andreev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DSjediList : UIViewController
@property (nonatomic, retain) IBOutlet UITableView *jediList;
@property (nonatomic, retain) NSMutableArray *jediMuArrey;
@end
