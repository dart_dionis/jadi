//
//  DSJediProfile.h
//  Jaday
//
//  Created by Denis Andreev on 20.08.13.
//  Copyright (c) 2013 Denis Andreev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DSJediProfile : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *jediPicture;
@property (nonatomic, retain) NSString *picName;
@property (strong, nonatomic) IBOutlet UITextView *jediInfo;
@property (nonatomic, retain) NSString *jediInfoText;
@property (strong, nonatomic) IBOutlet UINavigationItem *jediName;
@property (nonatomic, retain) NSString *jediNameText;

@end
